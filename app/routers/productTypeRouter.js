//khai báo thư viện 
const express = require("express");

const productTypeRouter = express.Router();

const {getAllProductTypes, createProductType, updateProductType, deleteProductType, getProductTypeById}=require('../controllers/productTypeController');
//create a productType
productTypeRouter.post("/productTypes", createProductType)
//get All ProductType
productTypeRouter.get("/productTypes",getAllProductTypes)
//getProductTypeById
productTypeRouter.get("/productTypes/:productTypeid",getProductTypeById)
//update productType
productTypeRouter.put("/productTypes/:productTypeid",updateProductType)

//delete productType
productTypeRouter.delete("/productTypes/:productTypeid", deleteProductType)


module.exports = productTypeRouter;