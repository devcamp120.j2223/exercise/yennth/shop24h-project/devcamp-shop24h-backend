const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const orderSchema = new Schema({
    _id:{
        type:mongoose.Types.ObjectId
    },
    orderDate:{
        type: Date,
        default:Date.now()
    },
    shipperDate:{
        type: Date
    },
    note:{
        type: String
    },
    orderDetail:[],
    cost:{
        type: Number,
        default:0
    },
    timeCreated:{
        type:Date,
        default:Date.now()
    },
    timeUpdated:{
        type:Date,
        default:Date.now()
    }
})
module.exports = mongoose.model("order", orderSchema);